.. image:: logo_fiuba.jpg
  :width: 40%
  :align: right

TP N°1
======
75.10 Técnicas de Diseño
========================
Refactoring: Giled Rose
-----------------------

:Nombre: Franetovich, Damián
:Padrón: 88924
:Cuatrimestre: Segundo 2013

.. raw:: pdf

      PageBreak

Diagrama de clases:
___________________

.. image:: class_diagram.png
  :width: 90%

Detalles de implementación:
___________________________

Al no poder modificar la interfaz pública de los objetos legacy (Inventory e Item), se creó un wrapper de Item, para poder asignarle el comportamiento de updateQuality, ya que por ser este un método que lee y modifica los atributos de la clase Item, debería estar contenido en esta. 
El wrapper (ItemWrapper) se creó con el fin de incorporar nuevo comportamiento a la clase Item, pero sin modificarla. De esta manera se fue detectando comportamiento repetido y se extrajo en distintos métodos privados.