package ar.fiuba.tecnicas.giledrose;

public class Inventory {
    private ItemWrapper[] items;

    public Inventory(Item[] items) {
        super();
        this.items = new ItemWrapper[items.length];
        for(int i = 0; i != items.length; i++) {
            this.items[i] = new ItemWrapper(items[i]);
        }
    }

    public Inventory() {
        super();
        items = new ItemWrapper[] {
                new ItemWrapper(new Item("+5 Dexterity Vest", 10, 20)),
                new ItemWrapper(new Item("Aged Brie", 2, 0)),
                new ItemWrapper(new Item("Elixir of the Mongoose", 5, 7)),
                new ItemWrapper(new Item("Sulfuras, Hand of Ragnaros", 0, 80)),
                new ItemWrapper(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20)),
                new ItemWrapper(new Item("Conjured Mana Cake", 3, 6))
        };

    }

    public void updateQuality() {
        for (ItemWrapper item: items) {
            item.update();
        }
    }
}
