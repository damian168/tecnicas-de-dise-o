package ar.fiuba.tecnicas.giledrose;

public class ItemWrapper {

    public static final int MAX_QUALITY = 50;
    private final Item item;

    public ItemWrapper(Item item) {
        this.item = item;
    }

    public String getName() {
        return item.getName();
    }

    public void setName(String name) {
        item.setName(name);
    }

    public int getSellIn() {
        return item.getSellIn();
    }

    public void setSellIn(int sellIn) {
        item.setSellIn(sellIn);
    }

    public int getQuality() {
        return item.getQuality();
    }

    private void setQuality(int quality) {
        item.setQuality(quality);
    }

    public void update() {
        updateSellIn();
        updateQuality();
    }

    private void updateQuality() {
        if (isQualityIncreasedByTime()) {
            incrementQuality();
        } else {
            decrementQuality();
        }

        if (isEventTicket() && isExpired()) {
            this.setQuality(0);
        }

    }

    private boolean isEventTicket() {
        return this.getName() == "Backstage passes to a TAFKAL80ETC concert";
    }

    private boolean isQualityIncreasedByTime() {
        return isVintage() || isEventTicket();
    }

    private boolean isVintage() {
        return this.getName() == "Aged Brie";
    }

    private void decrementQuality() {
        if (this.getQuality() > 0 && !isLegendary()) {
            this.setQuality(this.getQuality() - getDecrementByType());
        }
    }


    private void incrementQuality() {
        if (!haveMaxQuality()) {
            this.setQuality(this.getQuality() + getIncrementByType());
        }
    }

    private boolean haveMaxQuality() {
        return this.getQuality() >= MAX_QUALITY;
    }

    private Integer getIncrementByType() {
        if (isEventTicket()) {
            if (this.getSellIn() < 5) {
                return 3;
            }
            if (this.getSellIn() < 10) {
                return 2;
            }
        } else if(isVintage() && isExpired()){
            return 2;
        }
        return 1;
    }

    private Integer getDecrementByType() {
        if (isVolatile()) {
            return 2;
        }
        return 1;
    }

    private boolean isExpired() {
        return this.getSellIn() < 0;
    }

    boolean isVolatile() {
        return this.getName() == "Conjured";
    }


    private boolean isLegendary() {
        return this.getName() == "Sulfuras, Hand of Ragnaros";
    }

    private void updateSellIn() {
        if (!isLegendary()) {
            this.setSellIn(this.getSellIn() - 1);
        }
    }

}