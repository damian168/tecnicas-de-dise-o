package ar.fiuba.tecnicas.giledrose;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.*;


public class GiledRoseTest {

    @Test
    public void updateQualityTest_decrementaCalidad(){
        Item[] items = new Item[] {
                new Item("Normal item", 3, 8)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(7, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_quialityNuncaMenorACero(){
        Item[] items = new Item[] {
                new Item("Normal item", 3, 0)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(0, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_AgedBrieAumentaCalidad(){
        Item[] items = new Item[] {
                new Item("Aged Brie", 4, 8)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(9, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_TopeDeCalidad50(){
        Item[] items = new Item[] {
                new Item("Aged Brie", 3, 50)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(50, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_SulfurasCalidadConstante(){
        Item[] items = new Item[] {
                new Item("Sulfuras, Hand of Ragnaros", 0, 30)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(30, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_backstageIncrementaEn2_sellInMenorIguala10(){
        Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 8, 30)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(32, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_backstageIncrementaEn3_sellInMenorIguala5(){
        Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 30)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(33, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_backstageEnCero_sellInMenorIgualA0(){
        Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 0, 30)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(0, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_ConjuredDecrementaEn2(){
        Item[] items = new Item[] {
                new Item("Conjured", 8, 30)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(28, items[0].getQuality());
    }

    @Test
    public void updateQualityTest_SulfurasQualityConstante80(){
        Item[] items = new Item[] {
                new Item("Sulfuras, Hand of Ragnaros", 8, 80)
        };
        Inventory inventory = new Inventory(items);
        inventory.updateQuality();
        assertEquals(80, items[0].getQuality());
    }
}
