package ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl;

import ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl.LinkedQueue;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

public class LinkedQueueTest {

    private LinkedQueue<Integer> queue;

    @Before
    public void setUp() throws Exception {
        queue = LinkedQueue.newLinkedQueue();
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(queue.isEmpty());

        queue.add(1);
        assertFalse(queue.isEmpty());
    }

    @Test
    public void testSize() throws Exception {
        assertEquals(0, queue.size());

        queue.add(1);
        assertEquals(1, queue.size());

        queue.add(1);
        queue.add(2);
        assertEquals(3, queue.size());

        queue.add(2);
        assertEquals(4, queue.size());

        queue.remove();
        assertEquals(3, queue.size());
    }

    @Test
    public void testAdd() throws Exception {
        assertEquals(0, queue.size());

        queue.add(1);
        assertEquals(1, queue.size());
        assertEquals(1, queue.top().intValue());

        queue.add(1);
        queue.add(2);
        assertEquals(3, queue.size());

        queue.add(2);
        assertEquals(4, queue.size());
        assertEquals(1, queue.top().intValue());
    }

    @Test
    public void testTop() throws Exception {
        try {
            queue.top();
            fail("Exception should be throwed.");
        } catch (AssertionError e) {
        } catch (Exception e) {
            fail("Unexpected exception" + e.toString());
        }
        queue.add(1);
        assertEquals(1, queue.top().intValue());
        queue.add(15);
        assertEquals(1, queue.top().intValue());
        queue.add(31);
        assertEquals(1, queue.top().intValue());
        queue.add(51);
        assertEquals(1, queue.top().intValue());
        queue.add(6);
        assertEquals(1, queue.top().intValue());
        queue.add(8);
        assertEquals(1, queue.top().intValue());
        queue.add(9);
        assertEquals(1, queue.top().intValue());
        queue.remove();
        assertEquals(15, queue.top().intValue());
    }

    @Test
    public void testRemove() throws Exception {
        try {
            queue.remove();
            fail("Exception should be throwed.");
        } catch (AssertionError e) {

        } catch (Exception e) {
            fail("Unexpected exception" + e.toString());
        }
        queue.add(34);
        queue.add(4);
        queue.add(3);
        queue.add(6);
        queue.add(3);
        queue.add(37);
        queue.add(8);
        queue.add(4);
        queue.add(88);
        queue.add(0);
        assertEquals(34, queue.top().intValue());
        queue.remove();
        assertEquals(4, queue.top().intValue());
        queue.remove();
        assertEquals(3, queue.top().intValue());
        queue.remove();
        assertEquals(6, queue.top().intValue());
        queue.remove();
        assertEquals(3, queue.top().intValue());
        queue.remove();
        assertEquals(37, queue.top().intValue());
        queue.remove();
        assertEquals(8, queue.top().intValue());
        queue.remove();
        assertEquals(4, queue.top().intValue());
        queue.remove();
        assertEquals(88, queue.top().intValue());
        queue.remove();
        assertEquals(0, queue.top().intValue());
    }

    @Test
    public void testNewLinkedQueue() throws Exception {
        assertNotNull(LinkedQueue.newLinkedQueue());
    }
}
