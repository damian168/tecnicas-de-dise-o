package ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl;

import ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl.LinkedQueueNode;

public class NullLinkedNode<T> implements LinkedNode<T> {

    private static LinkedNode instance = new NullLinkedNode();

    private NullLinkedNode(){}

    @Override
    public LinkedNode<T> getNext() {
        throw new AssertionError("Queue is empty!");
    }

    @Override
    public Integer size() {
        return 0;
    }

    @Override
    public LinkedNode<T> add(T item) {
        return new LinkedQueueNode(item);
    }

    @Override
    public T getItem() {
        throw new AssertionError("Queue is empty!");
    }

    public static <T> LinkedNode<T> instance() {
        return instance;
    }
}
