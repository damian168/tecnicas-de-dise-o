package ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl;

import ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl.LinkedNode;
import ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl.NullLinkedNode;

public class LinkedQueueNode<T> implements LinkedNode<T> {

    private final T item;
    private LinkedNode<T> next = NullLinkedNode.instance();

    public LinkedQueueNode(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public LinkedNode<T> getNext() {
        return next;
    }

    public Integer size() {
        return 1 + getNext().size();
    }

    public LinkedNode<T> add(T newItem) {
        next = next.add(newItem);
        return this;
    }
}
