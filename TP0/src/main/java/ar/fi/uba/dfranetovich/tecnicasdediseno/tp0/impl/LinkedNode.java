package ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl;

public interface LinkedNode<T> extends Node<T> {

    LinkedNode<T> getNext();

    Integer size();

    LinkedNode<T> add(T item);

}
