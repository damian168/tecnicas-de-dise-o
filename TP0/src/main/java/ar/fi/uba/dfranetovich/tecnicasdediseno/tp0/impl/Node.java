package ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl;

public interface Node<T> {

    T getItem();

}
