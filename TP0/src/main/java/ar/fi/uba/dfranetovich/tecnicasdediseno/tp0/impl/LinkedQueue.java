package ar.fi.uba.dfranetovich.tecnicasdediseno.tp0.impl;

public class LinkedQueue<T>{

    private LinkedNode<T> first = NullLinkedNode.instance();

    public boolean isEmpty() {
        return first.size() == 0;
    }

    public int size() {
        return first.size();
    }

    public void add(T item) {
        first = first.add(item);
    }

    public T top() {
        return first.getItem();
    }

    public void remove() {
        first = first.getNext();
    }

    public static <T> LinkedQueue<T> newLinkedQueue() {
        return new LinkedQueue<T>();
    }

}
